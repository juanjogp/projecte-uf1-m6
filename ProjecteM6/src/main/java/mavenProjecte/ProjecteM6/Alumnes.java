package mavenProjecte.ProjecteM6;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement (name="institut")
public class Alumnes {
	private String nom;
	private ArrayList<Alumne> alumne = new ArrayList<Alumne>();
	
	public Alumnes() {
		super();
	}
	
	public Alumnes(String nom, ArrayList<Alumne> alumne) {
		super();
		this.nom = nom;
		this.alumne = alumne;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@XmlElementWrapper(name = "alumnes")
    @XmlElement(name = "alumne")
	public ArrayList<Alumne> getAlumne() {
		return alumne;
	}
	public void setAlumne(ArrayList<Alumne> alumne) {
		this.alumne = alumne;
	}

	@Override
	public String toString() {
		return "Alumnes [nom=" + nom + ", alumne=" + alumne + "]";
	}
	
	
}
