package mavenProjecte.ProjecteM6;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.simple.JSONObject;

public class Classe implements Serializable{

	private static final long serialVersionUID = 1L;
	private String Professor;
	private ArrayList<Alumne> alumne;
	private JSONObject aula;
	
	public Classe(String professor, ArrayList<Alumne> alumne, JSONObject aula) {
		super();
		Professor = professor;
		this.alumne = alumne;
		this.aula = aula;
	}

	public Classe() {
		super();
	}

	public String getProfessor() {
		return Professor;
	}

	public void setProfessor(String professor) {
		Professor = professor;
	}

	public ArrayList<Alumne> getAlumne() {
		return alumne;
	}

	public void setAlumne(ArrayList<Alumne> alumne) {
		this.alumne = alumne;
	}

	public JSONObject getAula() {
		return aula;
	}

	public void setAula(JSONObject aula) {
		this.aula = aula;
	}

	@Override
	public String toString() {
		return "Classe [Professor=" + Professor + ", alumne=" + alumne + ", aula=" + aula + "]";
		
	}
	
	
}
