package mavenProjecte.ProjecteM6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.xml.bind.Unmarshaller;

public class Main {

	public static void main(String[] args) throws Exception {

		Alumnes alum = llegirXML();
		/*
		 * System.out.println(alum.getNom() + " " + alum.getAlumne().get(0).getNom() +
		 * " " + alum.getAlumne().get(0).getTelefons().get(0));
		 * 
		 * 
		 * ;
		 */ 
		  ArrayList<Integer> telef = new ArrayList<Integer>(); telef.add(999999999);
		  telef.add(666666666);
		  
		  Alumne al = new
		  Alumne("Juanjo","Garcia","654564D","ajkashdjla",telef,"alkjdasd");
		 
		  afegeixAlumne(al);
		 

		 afegeixTelefon("28923792Z",777777);

		 alCarrer("654564D");

		 ArrayList<String> profes = llegirTXT();
		/*
		 * afegirProfessor("Profe Nou"); jubilarProfessor("Santamaría, Gregorio");
		 */

		// llegirJSON();
		// comprarMaquina("C4", "Nuevo", "i9", true);
		// canviaMaquina("Nuevo","1.6");
		// switchAC("C4");

		//crearClasse();
		llegirClasse();
	}

	// XML
	public static Alumnes llegirXML() {

		try {
			File file = new File("alumnes.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Alumnes.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Alumnes alum = (Alumnes) jaxbUnmarshaller.unmarshal(file);

			return alum;
			// System.out.println(alum.getAlumne());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// XML
	public static void afegeixAlumne(Alumne al) throws Exception {
		Alumnes ins = llegirXML();

		ins.getAlumne().add(al);

		escriureAlumne(ins);
	}

	// XML
	public static void afegeixTelefon(String dniAlumne, int telefon) throws Exception {
		Alumnes alum = llegirXML();

		for (int i = 0; i < alum.getAlumne().size(); i++) {
			if (alum.getAlumne().get(i).getDNI().equals(dniAlumne)) {
				alum.getAlumne().get(i).getTelefons().add(telefon);
			}
		}

		escriureAlumne(alum);
	}

	// XML
	public static void escriureAlumne(Alumnes ins) throws Exception {
		try {
			JAXBContext contextObj = JAXBContext.newInstance(Alumnes.class);

			Marshaller marshallerObj = contextObj.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

			FileOutputStream fos = new FileOutputStream("alumnes.xml");

			marshallerObj.marshal(ins, fos);
		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

	// XML
	public static void alCarrer(String dniAlumne) throws Exception {
		Alumnes alum = llegirXML();

		for (int i = 0; i < alum.getAlumne().size(); i++) {
			if (alum.getAlumne().get(i).getDNI().equals(dniAlumne)) {
				alum.getAlumne().remove(i);
			}
		}

		escriureAlumne(alum);
	}

	// TXT
	public static ArrayList<String> llegirTXT() {
		try {
			FileReader fr = new FileReader("professors.txt");
			BufferedReader br = new BufferedReader(fr);

			ArrayList<String> profes = new ArrayList<String>();
			String profe;
			while (br.ready()) {
				profe = br.readLine();
				profes.add(profe);
			}
			br.close();
			fr.close();

			Collections.sort(profes);

			return profes;

		} catch (Exception pasanCosas) {
			pasanCosas.printStackTrace();
			return null;
		}

	}

	// TXT
	public static void afegirProfessor(String professor) {
		ArrayList<String> profes = llegirTXT();

		profes.add(professor);
		Collections.sort(profes);
		try {
			File f = new File("professors.txt");
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			for (String s : profes) {
				System.out.println(s);
				bw.write(s + "\n");
			}
			bw.flush();
			bw.close();
			fw.close();

		} catch (Exception pasanCosas) {
			pasanCosas.printStackTrace();
		}

	}

	// TXT
	public static void jubilarProfessor(String professor) {
		ArrayList<String> profes = llegirTXT();

		profes.remove(professor);
		try {
			File f = new File("professors.txt");
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			for (String s : profes) {
				System.out.println(s);
				bw.write(s + "\n");
			}
			bw.flush();
			bw.close();
			fw.close();

		} catch (Exception pasanCosas) {
			pasanCosas.printStackTrace();
		}

	}

	// JSON
	public static void llegirJSON() {
		// TODO
		JSONParser parser = new JSONParser();

		try {
			// magia negra
			Object obj = parser.parse(new FileReader("aules.json"));

			JSONArray jsonArray = (JSONArray) obj;
			System.out.println(jsonArray);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	// JSON
	public static void comprarMaquina(String nomAula, String nomMaquina, String processador, boolean grafica) {
		try {

			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader("aules.json"));

			JSONArray array = (JSONArray) obj;

			for (Object o : array) {
				JSONObject aula = (JSONObject) o;

				if (aula.get("nom").equals(nomAula)) {
					JSONArray maquines = (JSONArray) aula.get("maquines");

					JSONObject ob = new JSONObject();

					ob.put("nom", nomMaquina);
					ob.put("processador", processador);
					ob.put("grafica", grafica);

					maquines.add(ob);

					System.out.println(ob);
				}
			}

			FileWriter file = new FileWriter("aules.json");
			file.write(array.toJSONString());
			file.flush();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// JSON
	public static void canviaMaquina(String nomMaquina, String nomAula) {
		try {
			JSONObject pc = new JSONObject();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader("aules.json"));

			JSONArray array = (JSONArray) obj;

			for (Object o : array) {
				JSONObject aula = (JSONObject) o;

				JSONArray maquines = (JSONArray) aula.get("maquines");

				for (int i = 0; i < maquines.size(); i++) {
					Object oo = null;
					JSONObject maquina = (JSONObject) maquines.get(i);

					System.out.println("TEST");
					System.out.println(maquines.get(i));

					if (maquina.get("nom").equals(nomMaquina)) {
						pc = maquina;
						System.out.println("Maquina original");
						System.out.println(maquina.get(i));

						maquines.remove(i);
					}
				}
			}

			for (Object o : array) {
				JSONObject aula = (JSONObject) o;
				JSONArray maquines2 = (JSONArray) aula.get("maquines");

				if (aula.get("nom").equals(nomAula)) {
					maquines2.add(pc);

					System.out.println("Maquina nuevo");
					System.out.println(pc);
				}
				System.out.println(o);
			}

			FileWriter file = new FileWriter("aules.json");
			file.write(array.toJSONString());
			file.flush();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// JSON
	public static void switchAC(String nomAula) {
		try {

			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader("aules.json"));

			JSONArray array = (JSONArray) obj;

			for (Object o : array) {
				JSONObject aula = (JSONObject) o;

				if (aula.get("nom").equals(nomAula)) {
					if (aula.get("aireacondicionat").equals(true)) {
						aula.put("aireacondicionat", false);
					} else {
						aula.put("aireacondicionat", true);
					}

				}
			}

			FileWriter file = new FileWriter("aules.json");
			file.write(array.toJSONString());
			file.flush();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Object llegirJSON2() {
		try {

			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader("aules.json"));

			return obj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void crearClasse() {
		// Alumnes
		ArrayList<Alumne> alumnos = new ArrayList<Alumne>();
		Alumnes alum = llegirXML();

		Collections.shuffle(alum.getAlumne());

		for (int i = 0; i < 5; i++) {
			alumnos.add(alum.getAlumne().get(i));
		}

		// Profes
		ArrayList<String> profes = llegirTXT();

		Collections.shuffle(profes);

		String profe = profes.get(0);
		// System.out.println(profes.get(0));

		// Aules
		JSONArray aules = (JSONArray) llegirJSON2();
		Collections.shuffle(aules);

		JSONObject aula = (JSONObject) aules.get(0);

		// System.out.println(aula.get("nom"));

		// Crear classe
		Classe classe = new Classe(profe, alumnos, aula);

		//System.out.println(classe.getProfessor()+","+classe.getAlumne().get(0).getNom()+","+classe.getAula());
		try {
			// File f = new File("classe.dat");
			FileOutputStream fol = new FileOutputStream("classe.dat");
			ObjectOutputStream oos = new ObjectOutputStream(fol);

			oos.writeObject(classe);

			fol.close();
			oos.close();

		} catch (Exception o) {
			o.printStackTrace();
		}
	}
	
	public static void llegirClasse() {
		try {
			FileInputStream fis = new FileInputStream("classe.dat");
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (true) {
				Classe c = (Classe)ois.readObject();
				System.out.println("Professor: ");
				System.out.println(c.getProfessor());
				System.out.println(" ");
				System.out.println("Alumnes:");
				for (int i = 0;i<c.getAlumne().size();i++) {
					System.out.println(c.getAlumne().get(i).getCognoms()+", "+c.getAlumne().get(i).getNom());
				}
				System.out.println(" ");
				System.out.println("Aula:");
				System.out.println(c.getAula());
			}
			
		} catch (EOFException eof) { //si sale esta excepcion ha ido bien la cosa
			//System.out.println("this is fine");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
