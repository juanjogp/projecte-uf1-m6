package mavenProjecte.ProjecteM6;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="alumnes")
public class Alumne implements Serializable{
	private static final long serialVersionUID = 1L;
	private String nom;
	private String cognoms;
	private String DNI;
	private String adreca;
	private ArrayList<Integer> telefons = new ArrayList<Integer>();
	private String mail;
	
	public Alumne() {
		super();
	}

	public Alumne(String nom, String cognoms, String dNI, String adreca, ArrayList<Integer> telefons, String mail) {
		this.nom = nom;
		this.cognoms = cognoms;
		DNI = dNI;
		this.adreca = adreca;
		this.telefons = telefons;
		this.mail = mail;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognoms() {
		return cognoms;
	}

	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}

	public String getAdreca() {
		return adreca;
	}

	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}
	
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@XmlElementWrapper(name = "telefons")
    @XmlElement(name = "telefon")
	public ArrayList<Integer> getTelefons() {
		return telefons;
	}

	public void setTelefons(ArrayList<Integer> telefons) {
		this.telefons = telefons;
	}
	
	

	
}
